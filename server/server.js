const express = require("express");
const bodyParser = require("body-parser");
const swaggerJSDoc = require("swagger-jsdoc");
const pathToSwaggerUi = require("swagger-ui-dist").absolutePath();
const fs = require("fs");

const app = express();
app.use(bodyParser.json());

app.use(express.static("public"));

const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Javascript forms training API",
    version: "1.0.0",
    description: "API for testing the forms made in frontend"
  },
  host: `localhost:8080`,
  basePath: "/"
};

const options = {
  swaggerDefinition,
  apis: ["./*.yaml"]
};

app.get("/api/docs/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerJSDoc(options));
});

app.use(
  "/docs",
  (req, res, next) => {
    const path = "http://localhost:8080/api/docs/swagger.json";

    const file = fs.readFileSync(`${pathToSwaggerUi}/index.html`).toString();

    const changedFile = file.replace(
      "https://petstore.swagger.io/v2/swagger.json",
      path
    );

    fs.writeFileSync(
      `${pathToSwaggerUi}/index.html`,
      changedFile,
      "utf8",
      err => {
        if (err) return console.error(err);
      }
    );

    next();
  },
  express.static(pathToSwaggerUi)
);

app.get("/api/participants", (req, res) => {
  try {
    const participants = JSON.parse(fs.readFileSync("./db.json")).participants;
    res.status(200).send(participants);
  } catch (e) {
    console.error(e);
    res.status(500).send({ message: "Server error" });
  }
});

app.post("/api/participants", (req, res) => {
  try {
    let db = JSON.parse(fs.readFileSync("./db.json"));
    let errors = [];

    const participant = {
      id: db.participants[db.participants.length - 1].id + 1,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      age: req.body.age,
      specialization: req.body.specialization
    };

    if (!participant.firstName) {
      errors.push("Nu ati introdus prenumele");
    }

    if (!participant.lastName) {
      errors.push("Nu ati introdus numele");
    }

    if (
      db.participants.find(
        item =>
          item.firstName === participant.firstName &&
          item.lastName === participant.lastName
      )
    )
      errors.push("Participant deja inregistrat");

    if (!participant.specialization) {
      errors.push("Trebuie sa alegeti o specializare");
    } else if (
      !["Cibernetica", "Info RO", "Info EN", "Statistica"].includes(
        participant.specialization
      )
    )
      errors.push("Specializare invalida");

    if (participant.age && !participant.age.toString().match(/^[0-9]*$/)) {
      errors.push("Varsta trebuie sa contine doar cifre");
    } else {
      participant.age = parseInt(participant.age);
    }

    if (errors.length === 0) {
      db.participants.push(participant);
      fs.writeFileSync("db.json", JSON.stringify(db));
      console.log(
        `Participant ${participant.firstName} ${participant.lastName} had registered`
      );
      res.status(201).send({ message: `V-ati inscris cu succes` });
    } else {
      res.status(400).send({ errors: errors });
    }
  } catch (e) {
    console.error(e);
    res.status(500).send({ message: "Server error" });
  }
});

app.listen(8080, () => {
  console.log(`Server is running on http://localhost:8080`);
});
